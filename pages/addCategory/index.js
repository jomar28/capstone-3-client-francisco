import {useState,useEffect,useContext} from 'react'
import {Form,Button,Jumbotron} from 'react-bootstrap'
import {Container, Row, Col, Card} from 'react-bootstrap'
import Category from './../../components/Category'
import UserContext from './../../userContext'
import Swal from 'sweetalert2'


export default function addCategory(){

	const {user} = useContext(UserContext)
	const [categoryName, setCategoryName] = useState("")
	const [typeType,setTypeType] = useState("Income")
	const [allCategories, setAllCategories] = useState([])
	const [token,setToken] = useState("")

	useEffect(()=>{

		setToken(localStorage.getItem('token'))


	},[])


	useEffect(()=>{

		fetch(`${process.env.NEXT_PUBLIC_API_URL}/api/users/allCategories`,{
			headers: {
				'Authorization': `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			setAllCategories(data)
		})
	},[])

	const categoriesCards = allCategories.map(category => {
		return(
		 	<Category key = {category._id} categoryProp = {category} />
		)
	})

	

	function selectType(e){

	e.preventDefault();


	fetch(`${process.env.NEXT_PUBLIC_API_URL}/api/users/category`,{
		method :'POST',
		headers: {
			'Content-Type':'application/json',
			'Authorization': `Bearer ${token}`
		},

		body: JSON.stringify({
			id:  user.id,
			name: categoryName,
			type: typeType 
		})
	})

	.then(res => res.json())
	.then(data =>{

	if(data){

			let timerInterval
			Swal.fire({
			  title: 'Confirmed!',
			  html: 'Creating in <b></b> milliseconds.',
			  timer: 2000,
			  timerProgressBar: true,
			  didOpen: () => {
			    Swal.showLoading()
			    timerInterval = setInterval(() => {
			      const content = Swal.getContent()
			      if (content) {
			        const b = content.querySelector('b')
			        if (b) {
			          b.textContent = Swal.getTimerLeft()
			        }
			      }
			    }, 100)
			  },
			  willClose: () => {
			    clearInterval(timerInterval)
			  }
			}).then((result) => {
			  if (result.dismiss === Swal.DismissReason.timer) {
			  }
			})
		window.location.replace('/')

	}else {

		alert("error")
	}

})

	setCategoryName("");
	setTypeType("Income");

}

	return(

		token
		?
		<Row className="mt-5 addCategory-wrapper">
			<Col md={6}>
			<h2>Create Category</h2>
		    <Form onSubmit ={e => selectType(e)}>
				<Form.Group controlId="cname">
					<Form.Label>Category</Form.Label>
					<Form.Control type="text" placeholder="Enter Category Name"  value={categoryName} onChange={e=> setCategoryName(e.target.value)} required/>
				</Form.Group>
				<Form.Group controlId="selectionLabel">
				    <Form.Label>Type</Form.Label>
				    <Form.Control as="select" value={typeType} onChange={e => setTypeType(e.target.value)}>
				    	<option value="" disabled></option>
				    	<option>Income</option>
				    	<option>Expense</option>
				    </Form.Control>
					</Form.Group>
				<Button variant="primary" type="submit" className="btn-block click">Create</Button>	
	  		</Form>			
			</Col>
			<Col md={6}>
				<h2>Categories Overview</h2>
			{categoriesCards}
			</Col>
		</Row>
		:
		<Jumbotron className="nt-wrapper">
			<div className="aligner">
				<p className="text-center display-4 font-weight-bold">
				Hey, you must be logged in to see this page. If you don't have an account yet, <a href="/register" className="text-decoration-underline text-black reg-nt">register here.</a>
				</p>
			</div>
		</Jumbotron>

		)


}