import {Row,Col} from 'react-bootstrap'
import Image from 'next/image'
import Link from 'next/link'

export default function About(){

	return(
		<>
		<Row id="about-wrapper" className="text-white p-0">
			<Col md={6} className="offset-md-4 about-text">
				<div className="about-content mb-5">
					<p className="font-weight-bold mb-n1">i am jomar...</p>
					<p>an ex-banker, now a full stack web developer.</p>
					<p className="font-weight-bold mb-n1 mt-3">Kalupi...</p>
					<p>is a web application to monitor your finances. make better financial decisions today, <a href="/register" className="text-decoration-underline register-about">register now!</a> </p>
					<p></p>	
				</div>
			</Col>
		</Row>


		</>

		)

}