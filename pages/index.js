import {useState,useEffect} from 'react'
import Link from 'next/link'
import {Row,Col,Card,Container,Button} from 'react-bootstrap'

export default function Home() {

	const [token,setToken] = useState("")

	useEffect(() => {
		setToken(localStorage.getItem('token'))
	},[])

  return (
  	<>
  		{
  		token
  		?
		<Row id="home-selection">
			<Col md={4} className="offset-md-4">
				<Link href="/addCategory" type="button">
					<a className="text-white text-decoration-none">
						<Card className="click hvr-grow">
							<Card.Body>
								<Card.Text className="text-center">
									Create Category
								</Card.Text>
							</Card.Body>
						</Card>
					</a>
				</Link>
				<Link href="/addRecord" type="button" className="text-center">
					<a className="text-white text-decoration-none">
						<Card className="mt-2 click hvr-grow">
							<Card.Body>
								<Card.Text className="text-center text-white">
									Add Record
								</Card.Text>
							</Card.Body>
						</Card>
					</a>
				</Link>
			</Col>
		</Row>
		:
		<>
		<Row className="p-0">
			<Col className="welcome-con">
			<div className="welcome">
				<p className="text-center mb-n1 display-2 font-weight-bold">Welcome to Kalupi </p>
				<p className="text-center display-4">Track your cash flows to make better financial decisions. <a href="/register" className="text-decoration-none register-text">Register now!</a></p>
			</div>
			</Col>
		</Row>
		</>
		}
  	</>
  )
}
