import {useState,useEffect,useContext} from 'react'
import {Form,Button,Row,Col,Card} from 'react-bootstrap'
import Router from 'next/router'

import UserContext from '../../userContext'

import {GoogleLogin} from 'react-google-login'

export default function Login(){

	const {user,setUser} = useContext(UserContext)

	const [email,setEmail] = useState("")
	const [password,setPassword] = useState("")

	const [isActive,setIsActive] = useState(true)

	useEffect(()=>{

			if(email !== "" && password !== ""){

				setIsActive(true)

			} else {

				setIsActive(false)

			}

	},[email, password])

	function authenticate(e){

		e.preventDefault()

		fetch(`${process.env.NEXT_PUBLIC_API_URL}/api/users/login`,{

				method: "POST",
				headers: {

					'Content-Type': 'application/json'

				},
				body: JSON.stringify({

					email: email,
					password: password

				})

		})
		.then(res => res.json())
		.then(data => {

				if(data.accessToken){

					localStorage.setItem("token", data.accessToken)
					fetch(`${process.env.NEXT_PUBLIC_API_URL}/api/users/details`,{

						headers: {

							Authorization: `Bearer ${data.accessToken}`

						}

					})
					.then(res => res.json())
					.then(data => {

							// console.log(data)
							localStorage.setItem('email',data.email)
							localStorage.setItem('id',data._id)
							
							setUser({

								email: data.email,
								id: data._id

							})

					})		

					Router.push('/')

				} else {

					alert("Login Failed")
				}

		})

		//set the input states into their initial value
		setEmail("")
		setPassword	("")



	}

	function authenticateGoogleToken(response){
		
		fetch(`${process.env.NEXT_PUBLIC_API_URL}/api/users/verify-google-id-token`,{

			method: 'POST',
			headers: {

				'Content-Type': 'application/json'

			},
			body: JSON.stringify({

				tokenId: response.tokenId,
				accessToken: response.accessToken

			})

		})
		.then(res => res.json())
		.then(data => {
			
		
			if(typeof data.accessToken !== 'undefined'){

				localStorage.setItem('token', data.accessToken)
			
				fetch(`${process.env.NEXT_PUBLIC_API_URL}/api/users/details`,{

					headers: {

							'Authorization': `Bearer ${data.accessToken}`
					}

				})
				.then(res => res.json())
				.then(data => {
					console.log(data)
					localStorage.setItem('email', data.email)
					localStorage.setItem('id', data._id)
		
					setUser({

						email: data.email,
						id: data._id

					})

					Router.push('/')
				})

			} else {

							
				if(data.error === "google-auth-error"){

					
					alert("Google Authentication Failed")


				} else if(data.error === "login-type-error"){


					alert("Login Failed")

				}

			}


		})

	}

	
	return(

		<Row id="login-page">
			<Col md={5}>
				<p className="font-weight-bold text-center display-2 mt-5 welcome">Welcome to Kalupi</p>
			</Col>
			<Col md={6} className="offset-md-1">
				<Form onSubmit={e => authenticate(e)} className="mt-3">
				<Form.Group controlId="userEmail">
					<Form.Label>Email:</Form.Label>
					<Form.Control type="email" placeholder="Enter Email" value={email} onChange={e => setEmail(e.target.value)} required/>
				</Form.Group>
				<Form.Group controlId="userPassword">
					<Form.Label>Password:</Form.Label>
					<Form.Control type="password" placeholder="Enter Password" value={password} onChange={e => setPassword(e.target.value)} required/>
				</Form.Group>
				{
					isActive
					? <Button variant="primary" type="submit" className="btn-block click">Login</Button>
					: <Button variant="primary" disabled className="btn-block click">Login</Button>
				}

				<GoogleLogin 

					clientId="139417257047-l9mgr09qbbatav31eu7lq4blp5ne75o0.apps.googleusercontent.com"
					buttonText="Login with Google"
					onSuccess={authenticateGoogleToken}
					onFailure={authenticateGoogleToken}
					cookiePolicy={'single_host_origin'}
					className="w-100 text-center my-4 d-flex justify-content-center"

				/>
			</Form>
			</Col>
		</Row>
			
		)

}