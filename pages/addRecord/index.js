import {useState,useEffect,useContext} from 'react'
import {Form,Button,Jumbotron} from 'react-bootstrap'
import {Container, Row, Col, Card} from 'react-bootstrap'
import Swal from 'sweetalert2'

export default function addRecord(){

	const [typeType,setTypeType] = useState("Income")
	const [dynamicCategories,setDynamicCategories] = useState([])
	const [categoryType,setCategoryType] = useState("")
	const [amount,setAmount] = useState("")
	const [description,setDescription] = useState("")
	const [date,setDate] = useState("")
	const [token,setToken] = useState("")

	useEffect(() => {
		setToken(localStorage.getItem('token'))
	},[])


	useEffect(()=>{

		fetch(`${process.env.NEXT_PUBLIC_API_URL}/api/users/allCategories`,{
			headers: {
				'Authorization': `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {

		let incomeArray=[];
		let expenseArray=[];

		if(typeType === "Income"){

			data.filter(result => {
				if(result.type === "Income"){

						incomeArray.push({_id: result._id, name: result.name})
				}
			})

			setDynamicCategories(incomeArray)

		}else{

			data.filter(result => {
				if(result.type==="Expense"){

						expenseArray.push({_id: result._id, name: result.name})
				}
			})

			setDynamicCategories(expenseArray)
			}
		})
	},[typeType])

	const dynamicArr = dynamicCategories.map(category => {
		return <option key={category._id}>{category.name}</option>
	})


	function addTransaction(e){
		e.preventDefault()

		fetch(`${process.env.NEXT_PUBLIC_API_URL}/api/users/transaction`,{
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				'Authorization': `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				id: localStorage.getItem('id'),
				type: typeType,
				category: categoryType,
				amount: amount,
				description: description,
				recordedOn: date
			})
		})
		.then(res => res.json())
		.then(data => {

			let timerInterval
			Swal.fire({
			  title: 'Confirmed!',
			  html: 'Recording in <b></b> milliseconds.',
			  timer: 2000,
			  timerProgressBar: true,
			  didOpen: () => {
			    Swal.showLoading()
			    timerInterval = setInterval(() => {
			      const content = Swal.getContent()
			      if (content) {
			        const b = content.querySelector('b')
			        if (b) {
			          b.textContent = Swal.getTimerLeft()
			        }
			      }
			    }, 100)
			  },
			  willClose: () => {
			    clearInterval(timerInterval)
			  }
			}).then((result) => {
			  if (result.dismiss === Swal.DismissReason.timer) {
			  }
			})
			window.location.replace('/')
		})
	}

	return(
		token
		?
		<>
		<Row>
			<Col md={4} className="offset-md-4">
				<h2 className="mt-5">Add Record</h2>
  		<Form onSubmit={e => addTransaction(e)} >
		    <Form.Group controlId="Type1">
			    <Form.Label>Type</Form.Label>
			    <Form.Control as="select" value={typeType} onChange={e => setTypeType(e.target.value)}>
			    	<option value="" disabled></option>
			    	<option>Income</option>
			    	<option>Expense</option>
			    </Form.Control>
		    </Form.Group>
		     <Form.Group controlId="Category1">
			    <Form.Label>Category</Form.Label>
			    <Form.Control as="select" value={categoryType} onChange={e => setCategoryType(e.target.value)}>
			    <option value="" disabled></option>
			    {dynamicArr}
			    </Form.Control>
		    </Form.Group>
	    	<Form.Group controlId="Amount">
			    <Form.Label>Amount</Form.Label>
			    <Form.Control type="number" value={amount} onChange={e => setAmount(e.target.value)} required/>
		    </Form.Group>
		    <Form.Group controlId="Description">
			    <Form.Label>Description</Form.Label>
			    <Form.Control type="text" value={description} onChange={e => setDescription(e.target.value)} required/>
		    </Form.Group>
		    <Form.Group controlId="Description">
			    <Form.Label>Date</Form.Label>
			    <Form.Control type="date" value={date} onChange={e => setDate(e.target.value)} required/>
		    </Form.Group>
		    <Button className="btn-block click" type="submit">Record</Button>
	    </Form>  
			</Col>
		</Row>
		
	    </>
	    :
	    <Jumbotron className="nt-wrapper">
			<div className="aligner">
				<p className="text-center display-4 font-weight-bold">
				Hey, you must be logged in to see this page. If you don't have an account yet, <a href="/register" className="text-decoration-underline text-black reg-nt">register here.</a>
				</p>
			</div>
		</Jumbotron>
		)

}