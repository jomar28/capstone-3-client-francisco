import BarCharts from './../../components/BarCharts'
import PieChart from './../../components/PieChart'
import {Jumbotron} from 'react-bootstrap'
import {useState,useEffect} from 'react'

export default function Charts(){

	const [token,setToken] = useState("")

	useEffect(() => {
		setToken(localStorage.getItem('token'))
	},[])

	return(
		token
		?
		<>
			<h1 className="text-center my-5">Charts</h1>
			<PieChart />
			<BarCharts />
		</>
		:
		<Jumbotron className="nt-wrapper">
			<div className="aligner">
				<p className="text-center display-4 font-weight-bold">
				Hey, you must be logged in to see this page. If you don't have an account yet, <a href="/register" className="text-decoration-underline text-black reg-nt">register here.</a>
				</p>
			</div>
		</Jumbotron>
		)

}