import '../styles/globals.css'
import 'bootstrap/dist/css/bootstrap.min.css'
import {Container} from 'react-bootstrap'
import NavBar from './../components/NavBar'
import Footer from './../components/Footer'
import {UserProvider} from '../userContext'
import {useState,useEffect} from 'react'

function MyApp({ Component, pageProps }) {

	const [user,setUser] = useState({

      email: null,
      id: null

  })

  useEffect(()=>{

      setUser({

        email: localStorage.getItem('email'),
        id: localStorage.getItem('id')

      })

  },[])

  const unsetUser = () => {


    localStorage.clear()


    setUser({

      email: null

    })

  }

  return(
  	<>
  	<UserProvider value={{user,setUser,unsetUser}}>
		 <NavBar />
		 	<Container className="main-wrapper">
		 		<Component {...pageProps} />
		 	</Container>
	</UserProvider>
  <Footer />
	</>
  	) 
}

export default MyApp
