import {useEffect,useState,useContext} from 'react'
import {Form,Button,Col,Row} from 'react-bootstrap'
import Swal from 'sweetalert2'

//import router from nextJS for user redirection
import Router from 'next/router'

export default function Register(){

	/*What do we bind to our input to track user input in real time?*/
	const [firstName,setFirstName] = useState("")
	const [lastName,setLastName] = useState("")
	const [email,setEmail] = useState("")
	const [mobileNo,setMobileNo] = useState(0)
	const [password1,setPassword1] = useState("")
	const [password2,setPassword2] = useState("")

	/*state for conditionally rendering the submit button*/
	const [isActive,setIsActive] = useState(true)

	//will run on every change to our user's input
	useEffect(()=>{

		if((firstName !== "" && lastName !== "" && email !== "" && mobileNo !== "" && password1 !== "" && password2 !== "") && (
			password1 === password2) && (mobileNo.length === 11)){

			setIsActive(true)

		}else{

			setIsActive(false)

		}


	},[firstName,lastName,email,mobileNo,password1,password2])

	function registerUser(e){

		e.preventDefault()

		//check if an email already exists
		fetch(`${process.env.NEXT_PUBLIC_API_URL}/api/users/email-exists`,{

			method: 'POST',
			headers: {

				'Content-Type': 'application/json'

			},
			body: JSON.stringify({

				email: email

			})

		})
		.then(res => res.json())
		.then(data => {

			if(data === false){

				fetch(`${process.env.NEXT_PUBLIC_API_URL}/api/users/`,{

					method: 'POST',
					headers: {

						'Content-Type': 'application/json'

					},
					body: JSON.stringify({

						firstName: firstName,
						lastName: lastName,
						email: email,
						mobileNo: mobileNo,
						password: password1

					})

				})
				.then(res => res.json())
				.then(data => {

					if(data){

						let timerInterval
						Swal.fire({
						  title: 'Confirmed!',
						  html: 'Registering in <b></b> milliseconds.',
						  timer: 2000,
						  timerProgressBar: true,
						  didOpen: () => {
						    Swal.showLoading()
						    timerInterval = setInterval(() => {
						      const content = Swal.getContent()
						      if (content) {
						        const b = content.querySelector('b')
						        if (b) {
						          b.textContent = Swal.getTimerLeft()
						        }
						      }
						    }, 100)
						  },
						  willClose: () => {
						    clearInterval(timerInterval)
						  }
						}).then((result) => {
						  if (result.dismiss === Swal.DismissReason.timer) {
						  }
						})

						Router.push('/login')

					} else {

						let timerInterval
						Swal.fire({
						  title: 'Login failed!',
						  html: 'Closing in <b></b> milliseconds.',
						  timer: 2000,
						  timerProgressBar: true,
						  didOpen: () => {
						    Swal.showLoading()
						    timerInterval = setInterval(() => {
						      const content = Swal.getContent()
						      if (content) {
						        const b = content.querySelector('b')
						        if (b) {
						          b.textContent = Swal.getTimerLeft()
						        }
						      }
						    }, 100)
						  },
						  willClose: () => {
						    clearInterval(timerInterval)
						  }
						}).then((result) => {
						  if (result.dismiss === Swal.DismissReason.timer) {
						  }
						})


					}

				})

			} else {

				let timerInterval
				Swal.fire({
				  title: 'Email already exists!',
				  html: 'Closing in <b></b> milliseconds.',
				  timer: 2000,
				  timerProgressBar: true,
				  didOpen: () => {
				    Swal.showLoading()
				    timerInterval = setInterval(() => {
				      const content = Swal.getContent()
				      if (content) {
				        const b = content.querySelector('b')
				        if (b) {
				          b.textContent = Swal.getTimerLeft()
				        }
				      }
				    }, 100)
				  },
				  willClose: () => {
				    clearInterval(timerInterval)
				  }
				}).then((result) => {
				  if (result.dismiss === Swal.DismissReason.timer) {
				  }
				})

			}

		})

		setFirstName("")
		setLastName("")
		setEmail("")
		setMobileNo(0)
		setPassword1("")
		setPassword2("")



	}

	return (
		<Row>
			<Col md={6} className="offset-md-3">
				<Form onSubmit={e => registerUser(e)} className="my-5">
				<Form.Group controlId="userFirstName">
					<Form.Label>First Name</Form.Label>
					<Form.Control type="text" placeholder="Enter First Name" value={firstName} onChange={e => setFirstName(e.target.value)} required/>
				</Form.Group>

				<Form.Group controlId="userLastName">
					<Form.Label>Last Name</Form.Label>
					<Form.Control type="text" placeholder="Enter Last Name" value={lastName} onChange={e => setLastName(e.target.value)}/>
				</Form.Group>

				<Form.Group controlId="userEmail">
					<Form.Label>Email</Form.Label>
					<Form.Control type="email" placeholder="Enter Email" value={email} onChange={e => setEmail(e.target.value)} required/>
				</Form.Group>

				<Form.Group controlId="mobileNo">
					<Form.Label>Mobile Number</Form.Label>
					<Form.Control type="number" placeholder="Enter Mobile No." value={mobileNo} onChange={e => setMobileNo(e.target.value)} required/>
				</Form.Group>

				<Form.Group controlId="password1">
					<Form.Label>Password</Form.Label>
					<Form.Control type="password" placeholder="Enter Password" value={password1} onChange={e => setPassword1(e.target.value)} required/>
				</Form.Group>

				<Form.Group controlId="password2">
					<Form.Label>Confirm Password</Form.Label>
					<Form.Control type="password" placeholder="Confirm Password" value={password2} onChange={e => setPassword2(e.target.value)} required/>
				</Form.Group>

				{
					isActive
					?
					<Button variant="primary" type="submit" className="click">Register</Button>
					:
					<Button variant="primary" className="click" disabled>Register</Button>
				}
				
			</Form>
			</Col>
		</Row>
			
		)

}