import {useState,useEffect} from 'react'
import {Form,Button,Row,Col,Jumbotron} from 'react-bootstrap'

export default function Profile(){

	const [token,setToken] = useState("")
	const [firstName,setFirstName] = useState("")
	const [lastName,setLastName] = useState("")
	const [email,setEmail] = useState("")


	useEffect(() => {
		setToken(localStorage.getItem('token'))
	})

	fetch(`${process.env.NEXT_PUBLIC_API_URL}/api/users/details`,{
		headers: {
			'Authorization': `Bearer ${token}`
		}
	})
	.then(res => res.json())
	.then(data => {
		setFirstName(data.firstName)
		setLastName(data.lastName)
		setEmail(data.email)
	})

	return(
		token
		?
		<>
			<Row className="p-0 m-3">
				<Col className="profile profile-wrapper"> 
					<div className="userData profile-content">
						<p className="font-weight-bold text-white my-n1">{firstName} {lastName}</p>
						<p className="font-weight-bold text-white">{email}</p>
					</div>
				</Col>
			</Row>
		</>
		:
		<>
		<Jumbotron className="nt-wrapper">
			<div className="aligner">
				<p className="text-center display-4 font-weight-bold">
				Hey, you must be logged in to see this page. If you don't have an account yet, <a href="/register" className="text-decoration-underline text-black reg-nt">register here.</a>
				</p>
			</div>
		</Jumbotron>
		</>
		)

}