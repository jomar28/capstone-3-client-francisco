import {useState,useEffect} from 'react'
import {Container, Row, Col, Card} from 'react-bootstrap'
import {Form,Button,Jumbotron} from 'react-bootstrap'
import {Fragment} from 'react'

import Category from './../../components/Category'

export default function Transactions(){

const [allTransactions, setAllTransactions] = useState([])
const [search,setSearch] = useState("")
const [filteredTrans,setFilteredTrans] = useState([])
const [records,setRecords] = useState([])
const [positive,setPositive] = useState([])
const [negative,setNegative] = useState([])
const [selector,setSelector] = useState("All")
const [token,setToken] = useState()

useEffect(() => {

	setToken(localStorage.getItem('token'))

},[])

useEffect(()=>{


	fetch(`${process.env.NEXT_PUBLIC_API_URL}/api/users/allTransactions`,{
		headers: {
			'Authorization': `Bearer ${localStorage.getItem('token')}`
		}
	})
	.then(res => res.json())
	.then(data => {

		setAllTransactions(data)
	})
},[])

useEffect(() => {

	setFilteredTrans(
		allTransactions.filter(transaction => {
			let found = transaction.description.toLowerCase().includes(search.toLowerCase())
				return found
			})
		)

},[search,allTransactions])

useEffect(() => {

	fetch(`${process.env.NEXT_PUBLIC_API_URL}/api/users/allTransactions`,{
		headers: {
			'Authorization': `Bearer ${localStorage.getItem('token')}`
		}
	})
	.then(res => res.json())
	.then(data => {
		
		const allArr = []
		const incomesArr = []
		const expensesArr = []
		const empty = []

		if(selector === "Income"){
			data.filter(result => {
				if(result.type === "Income"){
					incomesArr.push(result)
				}
			})
			setRecords(incomesArr)
		} else if (selector === "Expenses"){
			data.filter(result => {
				if(result.type === "Expense"){
					expensesArr.push(result)
				}
			})
			setRecords(expensesArr)
		} else if (selector === "All"){
			data.filter(result => {
				allArr.push(result)
			})
			setRecords(allArr)
		}
	})

},[selector])

const recordsArr = records.map(record => {

	return <Card key={record._id} className="mt-3 cc-link">
				<Card.Body className="hvr-underline-from-center">
					<Card.Text className="font-weight-bold text-white"><h3>{record.category}</h3></Card.Text>
					{
						record.type === "Income"
						?
						<Card.Text className="mb-n1 text-success font-weight-bold">{record.type}: {record.amount}</Card.Text>
						:
						<Card.Text className="mb-n1 text-danger font-weight-bold">{record.type}: {record.amount}</Card.Text>
					}
					<Card.Text className="text-white">{record.description}</Card.Text>
				</Card.Body>
			</Card>
			
})

const finalFilter = filteredTrans.map(result => {

	return <Card key={result._id} className="mt-3 cc-link">
				<Card.Body className="hvr-underline-from-center">
					<Card.Text className="font-weight-bold text-white"><h3>{result.category}</h3></Card.Text>
					{
						result.type === "Income"
						?
						<Card.Text className="mb-n1 text-success font-weight-bold">{result.type}: {result.amount}</Card.Text>
						:
						<Card.Text className="mb-n1 text-danger font-weight-bold">{result.type}: {result.amount}</Card.Text>
					}
					<Card.Text className="text-white">{result.description}</Card.Text>
				</Card.Body>
			</Card>
})

const positiveData = records.map(pos => {
	if(pos.type == "Income"){
		return parseInt(pos.amount)
	}
})

const negativeData = records.map(neg => {
	if(neg.type == "Expense"){
		return -Math.abs(parseInt(neg.amount))
	}
})

const posNeg = positiveData.concat(negativeData)
const filteredPosNeg = posNeg.filter(data => {
	return data != undefined
})

const balance = filteredPosNeg.reduce((accumulator,currentValue) => {
	return accumulator + currentValue
},0)

	return(
		<>	
			{ 
				token
			?
			<Container className="transaction-wrapper">
				<h1 className="text-center my-5">Transactions</h1>
				<Row>
					<Col md={6} className="text-center">
						<h2>History</h2>
						<Form>
							<Form.Group controlId="transaction-filter">
							    <Form.Control as="select" value={selector} onChange={e => setSelector(e.target.value)}>
							    <option>All</option>
							    <option>Income</option>
							    <option >Expenses</option>
							    </Form.Control>
						    </Form.Group>
						</Form>
							<Jumbotron className="cc-link-link">
							<h4 className="text-white">Current Balance: {balance}</h4>
							{recordsArr}
							</Jumbotron>
					</Col>
					<Col md={6} className="text-center">
						<h2>Search</h2>
						<Form>
							<Form.Group controlId="transaction-category">
							    <Form.Control type="text" value={search} placeholder="Search Records" onChange={e => setSearch(e.target.value)} required />
							    
							    {
							    search != ""
							    ?
							    <Jumbotron className="mt-3 cc-link-link">
							    <h4 className="text-white">Records</h4>
							    {finalFilter}
							    </Jumbotron>
							    :
							    null
							    }
						    </Form.Group>
						</Form>
					</Col>
				</Row>
			</Container>
			:
			<Jumbotron className="nt-wrapper">
				<div className="aligner">
					<p className="text-center display-4 font-weight-bold">
					Hey, you must be logged in to see this page. If you don't have an account yet, <a href="/register" className="text-decoration-underline text-black reg-nt">register here.</a>
					</p>
				</div>
			</Jumbotron>
					
			}
		</>

		)

}