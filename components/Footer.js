import {useContext} from 'react'
import {MDBContainer,MDBFooter} from 'mdbreact'
import {Row,Col} from 'react-bootstrap'
import Link from 'next/link'

export default function Footer(){

	return(

		<Row className="font-small py-2 footer p-0 m-0">
			<Col>
				<MDBFooter>
				<div className="footer-copyright text-center py-3">
					<MDBContainer>
						Jomar Francisco | Full Stack Web Developer | <a href="/about" className="text-decoration-none text-white">About</a>
					</MDBContainer>
				</div>
			</MDBFooter>
			</Col>
		</Row>
		)

}