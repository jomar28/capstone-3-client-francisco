import {useContext} from 'react'
import {Navbar,Nav} from 'react-bootstrap'

import Link from 'next/link'

import UserContext from '../userContext'

export default function NavBar(){

	const {user} = useContext(UserContext)
	console.log(user)

	return(

		
			<Navbar bg="dark" expand="lg" id="navbar">
				<Link href="/">
					<a className="navbar-brand text-white">Kalupi</a>
				</Link>
				<Navbar.Toggle aria-controls="basic-navbar-nav" id="toggle" />
				<Navbar.Collapse id="basic-navbar-nav">
					<Nav className="ml-auto">
						{
							user.email
							?
							<>
								<Link href="/">
								<a className="nav-link text-white" role="button">Home</a>
								</Link>
								<Link href="/transactions">
								<a className="nav-link text-white" role="button">Transactions</a>
								</Link>
								<Link href="/charts">
								<a className="nav-link text-white" role="button">Charts</a>
								</Link>
								<Link href="/profile">
								<a className="nav-link text-white" role="button">Profile</a>
								</Link>
								<Link href="/logout">
								<a className="nav-link text-white" role="button">Logout</a>
								</Link>
							</>
							:
							<>
								<Link href="/login">
									<a className="nav-link text-white" role="button">Login</a>
								</Link>
								<Link href="/register">
									<a className="nav-link text-white" role="button">Register</a>
								</Link>
							</>
						}
							
					</Nav>
				</Navbar.Collapse>
			</Navbar>

		)

}